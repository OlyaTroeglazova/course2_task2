import org.junit.Assert;
import org.junit.Test;

public class FinanceReportTest {

    private Payment p1 = new Payment("ПерваяФамилия Имя Отчество", 12, 11, 2019, 567899);
    private Payment p2 = new Payment("ВтораяФамилия Имя Отчество", 4, 02, 2019, 5679453);
    private Payment p3 = new Payment("ТретьяФамилия Имя Отчество", 4, 04, 2018, 9874324);
    private Payment p4 = new Payment("ЧетвертаяФамилия Имя Отчество", 30, 11, 2017, 45678);
    private Payment p5 = new Payment("ПятаяФамилия Имя Отчество", 2, 10, 2019, 8758395);
    private Payment[] p = {p1, p2, p3, p4, p5};
    private FinanceReport financeReport = new FinanceReport(p, "Фамилия Имя Отчество", 31, 12, 2019);

    public FinanceReportTest() throws Exception {}

    @Test
    public void copyFinanceReportTest() throws Exception {
        FinanceReport copy = new FinanceReport(financeReport);
        Assert.assertEquals(copy.getPayment(2), financeReport.getPayment(2));
        financeReport.setPayment(2, new Payment("ДругаяФамилия Имя Отчество", 12, 10, 2018, 853839));
        Assert.assertNotEquals(copy.getPayment(2), financeReport.getPayment(2));
    }

    @Test
    public void financeReportProcessorTest() throws Exception {
        FinanceReport financeReport1 = FinanceReportProcessor.filterByLastName(this.financeReport,'П');
        FinanceReport financeReport2 = FinanceReportProcessor.filterBySum(this.financeReport, 5679453);
        Assert.assertEquals(financeReport1.getCount(), 2);
        Assert.assertEquals(financeReport2.getCount(), 2);
    }

}
