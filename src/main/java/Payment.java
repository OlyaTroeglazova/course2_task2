import java.util.Objects;

public class Payment {
    private String name;
    private int day;
    private int month;
    private int year;
    private int sum;

    public Payment(String name, int day, int month, int year, int sum) throws Exception {
        if(!checkDate(day, month, year)){
            throw new Exception("Неверная дата");
        }
        this.name = name;
        this.day = day;
        this.month = month;
        this.year = year;
        this.sum = sum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return day == payment.day &&
                month == payment.month &&
                year == payment.year &&
                sum == payment.sum &&
                Objects.equals(name, payment.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, day, month, year, sum);
    }

    @Override
    public String toString() {
        return "Платеж: [" +
                "Плательщик: " + name + '\'' +
                ", дата: " + day +
                "." + month +
                "." + year +
                ", сумма: " + sum / 100 +
                "руб. " + sum % 100 + "коп.]";
    }

    private static boolean checkDate(int day,int month,int year) {
        int [] days={31,28,31,30,31,30,31,31,30,31,30,31};
        if(month<0 || month>12 ||day<0 ||year <0) return false;
        if(month!=2 && day>days[month-1]) return false;
        if(month==2 && ((year%400==0 && day>29)||(year%400!=0 && day>28))) return false;
        return true;
    }
}
