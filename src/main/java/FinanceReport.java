public class FinanceReport {
    private Payment[] payments;
    private String name;
    private int day;
    private int month;
    private int year;

    public FinanceReport(Payment[] payments, String name, int day, int month, int year) throws Exception {
        if(!checkDate(day, month, year)){
            throw new Exception("Неверная дата");
        }
        this.payments = payments;
        this.name = name;
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public FinanceReport(FinanceReport financeReport) throws Exception {
        this.name = financeReport.name;
        this.day = financeReport.day;
        this.month = financeReport.month;
        this.year = financeReport.year;
        this.payments = new Payment[financeReport.payments.length];
        for(int i = 0; i<financeReport.payments.length; i++){
            this.payments[i] = new Payment(financeReport.getPayment(i).getName(), financeReport.getPayment(i).getDay(),
                    financeReport.getPayment(i).getMonth(), financeReport.getPayment(i).getYear(), financeReport.getPayment(i).getSum());
        }
    }

    public Payment getPayment(int i) {
        return payments[i];
    }

    public String getName() {
        return name;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public void setPayment(int i, Payment payment) {
        payments[i] = payment;
    }

    public int getCount(){
        return payments.length;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder("Автор: [" + name +
                ", дата: " + day +
                "." + month +
                "." + year +
                ", Платежи: [");
        for(Payment p: payments){
            s.append(p.toString());
        }
        return s + "]";
    }

    private static boolean checkDate(int day,int month,int year) {
        int [] days={31,28,31,30,31,30,31,31,30,31,30,31};
        if(month<0 || month>12 ||day<0 ||year <0) return false;
        if(month!=2 && day>days[month-1]) return false;
        if(month==2 && ((year%400==0 && day>29)||(year%400!=0 && day>28))) return false;
        return true;
    }

}
