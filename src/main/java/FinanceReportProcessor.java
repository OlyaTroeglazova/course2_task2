public class FinanceReportProcessor {

    public static FinanceReport filterByLastName(FinanceReport financeReport, char c) throws Exception {
        FinanceReport res = new FinanceReport(financeReport);
        int count = 0;
        for(int i = 0; i<res.getCount(); i++){
            if(res.getPayment(i).getName().startsWith(String.valueOf(c))){
                count++;
            }
        }
        Payment[] p = new Payment[count];
        for(int i = 0, j=0; i<res.getCount(); i++){
            if(res.getPayment(i).getName().startsWith(String.valueOf(c))){
                p[j] = res.getPayment(i);
                j++;
            }
        }
        res = new FinanceReport(p, res.getName(), res.getDay(), res.getMonth(), res.getYear());
        return res;
    }

    public static FinanceReport filterBySum(FinanceReport financeReport, int sum) throws Exception {
        FinanceReport res = new FinanceReport(financeReport);
        int count = 0;
        for(int i = 0; i<res.getCount(); i++){
            if(res.getPayment(i).getSum()<sum){
                count++;
            }
        }
        Payment[] p = new Payment[count];
        for(int i = 0, j=0; i<res.getCount(); i++){
            if(res.getPayment(i).getSum()<sum){
                p[j] = res.getPayment(i);
                j++;
            }
        }
        res = new FinanceReport(p, res.getName(), res.getDay(), res.getMonth(), res.getYear());
        return res;
    }
}
